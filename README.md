# README

## What is this?

This is an application that helps organizing and planning the
amateur radio club station we (intend to) operate on occasion of
70 years of [DARC district D](https://www.darc.de/der-club/distrikte/d).

## Setup

Clone this project and move into your clone's workdir:

```
git clone https://gitlab.com/4ham/clubcall_reservation
cd clubcall_reservation
```

Install Ruby 2.6.6 and all Ruby software needed.  You should be able
to do this by obtaining [RVM](https://rvm.io/) and then doing

```
rvm install ruby-2.6.6
rvm use --default ruby-2.6.6
gem install bundler # This may not be needed
bundle install
```

## Database setup

Install [Postgres](https://www.postgresql.org/download/) on your
development box.  I use version 11.7, that or anything newer should be
fine.  (You could get probably by with Postgres in a Docker container
and a bit of fiddeling; but if you went that route, you'd be on your
own.)

As user `postgres` in `psql` do something like:

```
create role d70_dev login createdb;
alter role d70_dev with encrypted password '...';
create role d70_test login createdb;
alter role d70_test with encrypted password '...';

```

Furthermore, edit `pg_hba.conf` to contain the following lines
(where `NN` is your installed PostgreSQL version).

### Linux

`/etc/postgresql/NN/main/pg_hba.conf`

```
local  d70_dev,postgres  d70_dev  password
local  d70_test,postgres d70_test  password

```

### Windows

`C:\Program Files\PostgreSQL\NN\data\pg_hba.conf`

```
host  d70_dev,postgres  d70_dev  samehost password
host  d70_test,postgres d70_test  samehost password
```

### Setting the user-variables

This is for development and test. I leave prod as an exercise for the reader.

Before starting the server, hand in secrets as follows (you need to
generate two long hex strings as secret key and pepper for devise, via
`bundle exec rails secret`):

#### Linux

```
export DEV_DB_NAME=d70_dev
export DEV_DB_HOST=/var/run/postgresql
export DEV_DB_PORT=5432
export DEV_DB_USERNAME=d70_dev
export DEV_DB_PASSWD='...'

export TEST_DB_NAME=d70_test
export TEST_DB_HOST=/var/run/postgresql
export TEST_DB_PORT=5432
export TEST_DB_USERNAME=d70_test
export TEST_DB_PASSWD='...'

export DEVISE_SECRET_KEY='...'

export DEVISE_PEPPER='...'
```

#### Windows

You can add the variables `DEV_DB_NAME, DEV_DB_USERNAME, ...` to the
Windows `environmental variables`.  You will probably need
`DEV_DB_HOST` and `TEST_DB_HOST` to assume the value `localhost`.

## Get the DB going

### Schema

```
bundle exec rake db:create db:schema:load
```

### Initial admin user

This is presently `dj3ei`, which needs to change.  But, for now, start
`bundle exec rails c` and type something like (hopefully choosing a better
password than `lorem`):


```
u = User.new(callsign: 'dj3ei', email: 'andreas@famsik.de', password: 'lorem', password_confirmation: 'lorem')
u.save                # If this returns false, try u.errors.messages
u.confirmation_token
```

If you want to actually see the email, see to it that a glaringly open SMTP server is listening
at `localhost:2525` and run `bundle exec rake jobs:workoff` .  (For the glaringly open SMTP server,
[Mailcatcher](https://mailcatcher.me/) might be a good choice, but I have not tried it.)

Now, start for the first time with `bundle exec rails s` and confirm your email by clicking the link
found in the email, which would be, `http://localhost:3000/ops/bestätigen?confirmation_token=...`

### Initial station

We presently need a station with id 1.  Give `bundle exec rails c` some instructions like:

```
s = Station.new(name: "da0hackathon", earliest_start: Time.utc(2020,3,1), latest_end: Time.utc(2020,6,1))
s.save # If this returns false, try s.errors.messages
```

### Initial sign-up riddle

With `bundle exec rails s` running, log in as `dj3ei`, navigate to
[http://localhost:3000/captcha_templates/new](http://localhost:3000/captcha_templates/new)
and invent one or more riddles (captchas) that you think radio amateurs will have considerably
less problems solving compared to computers.

### Sign up as a different user.

Sign up and sign in via the web.  To get past the email confirmation,
use a (more or less) real mailserver and `bundle exec rake
jobs:workoff` or else `User.last.confirmation_token` will prove helpful
(typed into `bundle exec rails c`).

### Your admin job

You will now also want to visit, as user `dj3ei`, the admin URI
[http://localhost:3000/4admin](http://localhost:3000/4admin).  Only
users that were verified via that URI are fully ready for action.

That is, they should now be able to reserve the station you entered.

## On production

Here comes a list of notes I took when starting production,
which is somewhat redundant with the above list.

This is a list of the kind of env variables you will want to use
on the production server:

```
export RAILS_ENV=production
export PROD_DB_USERNAME='...'
export PROD_DB_PASSWD='...'
export SMTP_SERVER='...'
export RAILS_LOG_TO_STDOUT=true
export PORT=4000

# To generate each of the following two: bundle exec rails secret
export DEVISE_SECRET_KEY=''
export DEVISE_PEPPER=''
```

## To get auth going

```
u = User.new(callsign: 'dj3ei', email: 'dj3ei@d-70.de', password: 'lorem', password_confirmation: 'lorem')
u.save
u.confirmation_token
```

Next, read your mail and click on the link, which should be along the general line of
`/ops/best%C3%A4tigen?confirmation_token=...`

Then hasten to put a few nice riddles into the captcha_template resource!

## Contributing

We welcome cooperation.

* Create a pull request as usual.

* All commits in this repository are GPG-signed, and we would prefer
  that to stay that way.

* We have historically neglected tests, but like to change that.
  As a minimum, `bundle exec rails test` should not fail.
