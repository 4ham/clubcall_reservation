require "application_system_test_case"

class CaptchaTemplatesTest < ApplicationSystemTestCase
  setup do
    @captcha_template = captcha_templates(:one)
  end

  test "visiting the index" do
    visit captcha_templates_url
    assert_selector "h1", text: "Captcha Templates"
  end

  test "creating a Captcha template" do
    visit captcha_templates_url
    click_on "New Captcha Template"

    fill_in "Answered", with: @captcha_template.answered
    fill_in "Answered Successfully", with: @captcha_template.answered_successfully
    fill_in "Asked", with: @captcha_template.asked
    fill_in "Question", with: @captcha_template.question
    fill_in "Solution Regexp", with: @captcha_template.solution_regexp
    fill_in "Valid", with: @captcha_template.valid
    click_on "Create Captcha template"

    assert_text "Captcha template was successfully created"
    click_on "Back"
  end

  test "updating a Captcha template" do
    visit captcha_templates_url
    click_on "Edit", match: :first

    fill_in "Answered", with: @captcha_template.answered
    fill_in "Answered Successfully", with: @captcha_template.answered_successfully
    fill_in "Asked", with: @captcha_template.asked
    fill_in "Question", with: @captcha_template.question
    fill_in "Solution Regexp", with: @captcha_template.solution_regexp
    fill_in "Valid", with: @captcha_template.valid
    click_on "Update Captcha template"

    assert_text "Captcha template was successfully updated"
    click_on "Back"
  end

  test "destroying a Captcha template" do
    visit captcha_templates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Captcha template was successfully destroyed"
  end
end
