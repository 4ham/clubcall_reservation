require "application_system_test_case"

class UserPreferencesTest < ApplicationSystemTestCase
  setup do
    @user_preference = user_preferences(:one)
  end

  test "visiting the index" do
    visit user_preferences_url
    assert_selector "h1", text: "User Preferences"
  end

  test "creating a User preference" do
    visit user_preferences_url
    click_on "New User Preference"

    fill_in "Anniversary Mail", with: @user_preference.anniversary_mail
    fill_in "April Hours", with: @user_preference.april_hours
    fill_in "Club Station", with: @user_preference.club_station
    fill_in "Club Station Use", with: @user_preference.club_station_use
    fill_in "General Mail", with: @user_preference.general_mail
    fill_in "Help Programming", with: @user_preference.help_programming
    fill_in "Help Qslcard", with: @user_preference.help_qslcard
    fill_in "Help Translate Paperlog", with: @user_preference.help_translate_paperlog
    fill_in "Home Station Use", with: @user_preference.home_station_use
    fill_in "Log Other Format", with: @user_preference.log_other_format
    fill_in "Log Paper", with: @user_preference.log_paper
    fill_in "Log Provide Adif", with: @user_preference.log_provide_adif
    fill_in "Log Via Web", with: @user_preference.log_via_web
    fill_in "My Clubstation Call", with: @user_preference.my_clubstation_call
    fill_in "My Clubstation Othercall April", with: @user_preference.my_clubstation_othercall_april
    fill_in "My Clubstation Othercall Whole Year", with: @user_preference.my_clubstation_othercall_whole_year
    fill_in "Only April", with: @user_preference.only_april
    fill_in "Ov", with: @user_preference.ov
    fill_in "User", with: @user_preference.user_id
    fill_in "Whole Year", with: @user_preference.whole_year
    fill_in "Whole Year Hours", with: @user_preference.whole_year_hours
    click_on "Create User preference"

    assert_text "User preference was successfully created"
    click_on "Back"
  end

  test "updating a User preference" do
    visit user_preferences_url
    click_on "Edit", match: :first

    fill_in "Anniversary Mail", with: @user_preference.anniversary_mail
    fill_in "April Hours", with: @user_preference.april_hours
    fill_in "Club Station", with: @user_preference.club_station
    fill_in "Club Station Use", with: @user_preference.club_station_use
    fill_in "General Mail", with: @user_preference.general_mail
    fill_in "Help Programming", with: @user_preference.help_programming
    fill_in "Help Qslcard", with: @user_preference.help_qslcard
    fill_in "Help Translate Paperlog", with: @user_preference.help_translate_paperlog
    fill_in "Home Station Use", with: @user_preference.home_station_use
    fill_in "Log Other Format", with: @user_preference.log_other_format
    fill_in "Log Paper", with: @user_preference.log_paper
    fill_in "Log Provide Adif", with: @user_preference.log_provide_adif
    fill_in "Log Via Web", with: @user_preference.log_via_web
    fill_in "My Clubstation Call", with: @user_preference.my_clubstation_call
    fill_in "My Clubstation Othercall April", with: @user_preference.my_clubstation_othercall_april
    fill_in "My Clubstation Othercall Whole Year", with: @user_preference.my_clubstation_othercall_whole_year
    fill_in "Only April", with: @user_preference.only_april
    fill_in "Ov", with: @user_preference.ov
    fill_in "User", with: @user_preference.user_id
    fill_in "Whole Year", with: @user_preference.whole_year
    fill_in "Whole Year Hours", with: @user_preference.whole_year_hours
    click_on "Update User preference"

    assert_text "User preference was successfully updated"
    click_on "Back"
  end

  test "destroying a User preference" do
    visit user_preferences_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "User preference was successfully destroyed"
  end
end
