require 'test_helper'

class UserTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "do not (for now) accept non-German callsigns" do
    u = users(:admin)
    u.callsign = "XY1Z"
    assert_not(u.save)
  end

  test "can promote verified member to admin" do
    u = users(:member)
    u.is_admin = true
    assert(u.save)
  end

  test "Creating a user sends out an email" do
    u = User.new(callsign: "dm2n", email: "op@op.org",
                 password: "mittel_geh_heim", password_confirmation: "mittel_geh_heim")
    assert_enqueued_with(job: ActionMailer::DeliveryJob) do
      assert(u.save)
    end
  end

end
