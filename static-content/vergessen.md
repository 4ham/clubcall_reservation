# Sie wollen vergessen werden?

Bis Ende März 2019 geht das noch ohne Wenn und Aber. (Wenn wir erst
einmal Logs von Verbindungen haben, wird das etwas schwieriger
werden.)

Bitte senden Sie eine Email-Anschrift an <a href="dj3ei@d-70.de">dj3ei</a>.
