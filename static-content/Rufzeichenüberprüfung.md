# Überprüfung - warum dauert das so lange?

Neuzugänge sehen auf ihrer Statusseite diesen Text:

> Sie warten auf eine Überprüfung durch das dr70bln-Team. Die sollte in
> zwei, drei Tagen erledigt sein. (Diese Überprüfung soll
> Rufzeichenmisbrauch verhindern.)

Und dann passiert ein, zwei, vielleicht drei Tage gar nichts.

Warum dauert das so lange?

## Die Betrugsgefahr, die wir abwenden.

Ein Gauner ohne Lizenz könnte sich irgendwo das Rufzeichen eines
nichtsahnenden Funkamateurs des Distrikts D besorgen.  Und eine
Emailanschrift.  Das Rufzeichen und die Emailanschrift brauchen nichts
miteinander zu tun zu haben.  Hat man beides, kann man sich hier
eintragen.  Und anschließend potentiell unter falscher Flagge segeln.

Wir wollen nicht, dass unser Klubstationsrufzeichen an einen
Schwarzfunker vergeben wird.

## Die Überprüfung

Nun sind wir ein Distrikt des DARC.  Der DARC hat eine
Mitgliederdatenbank.  Dort kann man (anhand der Rufzeichens)
nachschauen, ob eine Person Mitglied des DARC Distriktes D ist.  Und,
unter welcher Emailanschrift die Person zu erreichen ist (wenn die
Person dem DARC gegenüber eine Emailanschrift angegeben hat).

Wenn die Emailanschrift in der Mitglieder-DB des DARC mit der hier
registrierten übereinstimmt, ist alles gut.  Das Risiko, trotzdem
einem Schwarzfunker aufzusitzen, ist ausreichend gering.

Wenn die beiden Emailanschriften sich unterscheiden - gut. (Ich will
z.B. niemanden dazu zwingen, das DARC-Postfach ernsthaft zu benutzen.)
Also schicke ich dann eine überprüfende Nachricht an die beim DARC
gemeldete Anschrift.  Wenn die Empfängerin oder der Empfänger der
Nachricht mir bestätigt, dass es mit der Registrierung alles seine
Richtigkeit hat, ist auch alles gut.

## Aber: An die Daten herankommen!

Nur muss ich für diesen Vorgang (den ich manuell ausführe) an die
nötigen Daten aus der Mitgliederdatenbank des DARC herankommen.

Geht das?  Im Prinzip: Ja.

Wer Funktionsträger im DARC ist, kommt an Daten der Mitglieder heran.

Aber...

nur an die, die wirklich gebraucht werden.  Ich (DJ3EI) bin zum
Beispiel stellvertretender OVV des Ortsverbandes D25 Treptow/Köpenick.
In dieser Eigenschaft komme ich an die Daten der Mitglieder des OV D25
heran.  Aber nicht an alle anderen des Distriktes D.

Nein, eigentlich sind wir noch nicht bei "Radio Eriwan" angekommen.
Das ist noch nicht das "Aber".  Das ist so weit alles völlig
sachgerecht und korrekt, koscher und richtig.  DSGVO-konform.  So muss
das sein.  Man sieht nur die Daten, die man braucht.

## Kein Problem!

Nun brauche ich für diese Aktion ein paar mehr Daten als sonst.  Nicht
nur OV D25, sondern Distrikt D.  "Berechtigtes Interesse" nach DSGVO,
satzungsgemäße Clubinteressen fördern, das kann man rechtlich
problemlos argumentieren.

Distriktvorstand Marcus hat folgerichtig die DARC IT gebeten, mir für
die Dauer der Aktion (drei Monate) meinen Zugang zu erweitern, damit
ich den Zugriff habe, den ich brauche.  (Danke, Marcus!)

## Aber...

Das war dann doch ein Problem. Nun ist es so weit: Willkommen bei
Radio Eriwan.

Das hat die DARC IT nämlich nicht hingekriegt.  Oder nicht gewollt,
ich bin nicht sicher.

Das Problem der IT: Ich habe laut Mitgliederdatenbank nur ein Amt im
OV D25, nicht im Distrikt D.  Und (wenn ich das richtig verstehe),
sind dort, in der Datenbank, nur bestimmte Ämter
vorgesehen. "Beauftragter für die Jubiläumsfunkaktion" gehört nicht
dazu.  In das Raster der DARC-IT passt nicht, was ich hier treibe.
Fazit: Zugang?  Njet.

Nun arbeite ich selbst beruflich in der IT.  Bei meinen Kunden
versuche ich, möglich zu machen, was rechtlich und technisch geht.

Nun geben meine Kunden typischerweise mehr Geld für IT aus, als der
DARC dafür aufwenden kann.  Natürlich gilt: "You get what you pay
for."  Trotzdem kriege ich keine besonders gute Laune, wenn ich über
diese Geschichte nachdenke.

## Mit etwas Geduld geht es dann doch

Nun sind wir Berliner, und wir sind Funkamateure.  Die Berliner sind
ein kreatives Völkchen, und die Funkamateure sowieso.  Wir haben dann
doch noch einen Weg gefunden.

Eigentlich ganz einfach: Jemand mit einem DARC-IT-konformen Amt im
Distrikt hilft, schaut für mich in der Mitgliederdatenbank nach.

Nur ist er dadurch etwas lahm, der Vorgang:

* Erst muss ich sehen, dass jemand sich neu angemeldet hat.
* Dann Email-Nachricht hin,
* die muss wieder gesehen werden.
* Recherche.
* Email-Nachricht zurück
* die ich dann wieder sehen muss,
* mit etwas Pech (unterschiedliche Anschriften) Emailanschrift an den Neuzugang,
* der irgendwann antwortet,
* was ich dann wieder sehen muss.

Das eigentliche Freischalten ist dann nur noch ein Klick im Admin-Interface.

Also mehrfach die Verzögerung, die sich ergibt, weil wir alle auch
noch andere Dinge tun, als ununterbrochen auf unsere Emailpostfächer
zu starren.  So wird er (vergleichsweise) lahm, der Vorgang.

