# Die Registrierung funktioniert (zur Zeit) so:

## Registrieren

Das tun Sie, wenn wir Sie hier noch gar nicht kennen.
Sie sind dazu herzlich eingeladen, wenn Sie

* Lust haben, das Klubstationsrufzeichen dr70bln im
  April, Mai oder Juni 2019 zu aktivieren,

* Mitglied eines Ortsverbandes des Distrikts D sind und

* lizenzierter Funkamateur (im Amtsdeutsch: über eine in Deutschland
  gültige Zulassung zum Amateurfunk verfügen).

### Wie geht das?

Sie geben Ihr Rufzeichen ein, Ihre Email-Anschrift und zweimal
dasselbe Password.  Außerdem müssen Sie ein kleines Rätsel lösen,
das Ihnen als Funkamateur nicht schwerfallen sollte.

Darauf schicken wir Ihnen (sofort) eine Mail mit einem
Bestätigungslink.  Darauf klicken, um Ihre Email-Anschrift zu
bestätigen.

Wenn Sie dann immer noch Ihr Password wissen, kommen Sie rein.  Wenn nicht:

## Password reset

Mit einer registrierten Email können Sie Ihr Password auch neu setzen.
(Noch eine Email, noch mal klicken. Das Übliche halt.)

### Wenn Sie kein Funkamateur sind? Oder nicht aus D?

Interessant!  Wir hatten vermuteten, dass hier nur Funkamateure
aus dem Distrikt D mitmachen wollen.

Nur zu!  Schreiben Sie dem Webmaster [dj3ei](mailto:dj3ei@d-70.de)
eine Email und schildern die Einzelheiten Ihres Falls!

## Rufzeichenänderung?

Herzlichen Glückwunsch!

Als erstes bitte die
[Rufzeichenmeldung](https://www.darc.de/geschaeftsstelle/mitgliederverwaltung/#c153821)
dem DARC gegenüber absetzen!  Anschließend schreiben Sie dem Webmaster
[dj3ei](mailto:dj3ei@d-70.de) eine Email und
melden Sie den Fall!  (Leider ist _beides_ nötig.)
