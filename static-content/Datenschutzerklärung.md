## Datenschutzerklärung

### Verantwortlich

für diese Webseite und den Datenschutz ist

Andreas Krüger (dj3ei)     
Herweghstr. 13     
12487 Berlin     
Deutschland

Email-Anschrift: dj3ei@d-70.de, Telefon: +49 30 98 32 4321.

### Grundsätzlich

Wir sind sparsam beim Verarbeiten Ihrer personenbezogenen Daten.

Welche personenbezogene Daten wir erheben, hängt davon ab, ob sie
sich persönlich angemeldet haben oder nicht.

### Nicht-angemeldetes Surfen

#### IP-Adressen

Die personenbezogenen Daten, die von dieser Webseite verarbeitet
werden, sind IP-Adressen.

Unser Server nutzt diese Daten, um seine Antworten (die Inhalte) an
Sie zurück zu schicken.  (Jeder Webserver macht das so.)

Darüber hinaus anonymisieren wir die IP-Adressen und speichern die
anonymisierte Version in einem üblichen Server-Log.

Für eine gewissen Übergangszeit sind die IP-Adressen allerdings noch
nicht anonymisiert, sondern zunächst nur pseudonymisiert.

Wir benutzen ein technisches Verfahren, dass die anonymisierte
IP-Adresse aus der wirklichen IP-Adresse und einer Zufallszahl mit
Hilfe einer sogenannten “Hash”-Funktion ermittelt.  Die dafür benutzte
Zufallszahl wird 24 Stunden lang benutzt und nach 48 Stunden gelöscht.

So lange die Zufallszahl noch nicht gelöscht ist, ist die IP-Adresse
noch nicht völlig anonymisiert.  Sie ist nur pseudonymisiert.  Mit
Hilfe der Zufallszahl (und einigem Ausprobieren) könnte man die
ursprüngliche IP-Adresse zurück gewinnen - zumindest mit einiger
Wahrscheinlichkeit.

Nach 48 Stunden wird die Zufallszahl, die am Vortag in Benutzung war,
endgültig gelöscht.  Damit wird aus der Pseudonymisierung eine
Anonymisierung.  Die anonymisierte IP-Adresse im Server-Log ist dann
kein schützenswertes “personenbezogenes Datum” mehr.

Das von uns gewählte Anonymisierungsverfahren erlaubt es,
“Klickspuren” auf unseren Webseiten über Zeiträume von einige Stunden
hinweg zu verfolgen.  Das hilft bei der Optimierung des Angebots.  Uns
interessiert dabei nicht, welche konkrete Person die Klickspur
hinterlassen hat.

Die Optimierung unseres Angebots stellt ein berechtigtes Interesse im
Sinne der DSGVO dar.  Wir sind berechtigt, pseudonymisierte
IP-Adressen für 48 Stunden zu speichern.  Sie haben das Recht, sich
bei der zuständigen Aufsichtsbehörde darüber zu beschweren.

Uns interessiert nicht, welche konkrete Person die Klickspur
hinterlassen hat.  Für unsere Zwecke ist es daher nicht nötig, die
pseudonymisierten IP-Adressen während dieser ersten Stunden auf die
ursprünglichen, echten IP-Adressen zurückzuführen, so lange sie noch
nicht anonymisiert ist, und wir tun das auch nicht.  Um Ihnen auch
während dieser kurzen Zeit die üblichen Datenschutzrechte zu gewähren
(Auskunft, Löschung und so weiter, ob Ihre echte IP-Nummer sich bei
uns findet), müssten wir (nur dafür!)  eine solche Möglichkeit zur
Endpseudonymisierung aufbauen.  Das zu tun wäre offensichtlich
kontraproduktiv für den Datenschutz.  Das sehen die
Datenschutzregelungen auch so.  Wir sind daher nicht dazu
verpflichtet.

### Cookies

Wir benutzen Session-Cookies.  Im Session-Cookie überträgt unsere
Webanwendung Informationen, die von ihr später wieder gebraucht
werden.  (Deswegen ändert sich der Wert auch häufig.)

Wenn der Benutzer nicht angemeldet ist, enthalten unsere Session
Cookies keine personenbezogenen Daten.

Häufig werden Cookies dazu angewendet, um User zu tracken und zu
ent-anonymisieren.  Wir benutzen unsere Session-Cookies nicht zu
diesen Zwecken.  Wir lagern Informationen in das Session-Cookie aus
und lassen diese Informationen über Ihren Browser laufen.  Genau
deshalb brauchen wir diese Informationen im eigenen Datenbestand nicht
zu speichern.  Im Endeffekt erhöht unser Verfahren Ihre Anonymität
unserer Anwendung gegenüber.

## Angemeldetes Surfen

Wenn Sie sich registrieren, Ihre Email-Anschrift bestätigen oder sich
anmelden, identifizieren Sie sich dem System gegenüber.  Jede
Interaktion zwischen Ihnen und unseren Servern ist damit unmittelbar
Verarbeitung personenbezogener Daten.

## Cookies und angemeldetes Surfen

So lange Sie angemeldet mit unserer Webanwendung arbeiten, enthält das
Session-Cookie Daten, anhand derer Sie unmittelbar identifiziert
werden können.

## Speicherung von Daten bei ändernder Interaktion

Wenn Sie mit unserer Webanwendung _ändernd_ interagieren, übertragen
Sie Daten, die personenbezogen gespeichert werden. Sie erkennen
ändernde Interaktionen daran, dass eins von diesen beiden Dingen der
Fall ist:

* Sie klicken auf ein Link in einer Email-Nachricht, die Sie von uns
  zur Bestätigung Ihrer Email-Anschrift erhalten haben.

* Sie drücken in der UI der Anwendung auf einen Knopf oder eine
  Schaltfläche.

Ändernde Interaktion führt zur (langfristigen) Speicherung von
personenbezogenen Daten auf unseren Systemen:

* Wir speichern Ihre Interaktionsdaten in unserer Datenbank, um den
Zweck erfüllen zu können, für den Sie uns die Daten übertragen.

* Wir speichern Daten außerdem in Anwendungslogs, um bei etwaigen
Problemen die Datenbankinhalte wiederherstellen zu können.

Sie haben die üblichen Rechte auf Auskunft, Korrektur oder Löschung
Ihrer personenbezogenen Daten von unseren Systemen.

### QSO-Daten

Wenn in unserem System Daten vorliegen, die Planung und Durchführung
von Funkverkehr mit von uns verwalteten Klubstationsrufzeichen
betreffen, werden wir eine Löschung des personenbezogenen Anteils erst
nach Ende der Verjährung von Ordnungswidrigkeiten nach dem AfuG
vornehmen.

Um weiterhin QSL-Karten und ähnliche Anfragen beantworten zu können,
werden wir die nunmehr anonymen QSO-Daten auch danach noch
aufbewahren, ohne besondere Frist.

## IP-Nummern und angemeldetes Surfen

Wir haben zwei verschiedene Logs: Serverlogs und Anwendungslogs.
(Technisch handelt es sich beim &ldquo;Server&rdquo; um einen
sogenannten &ldquo;Reverse Proxy&rdquo;.)

Normalerweise gleichen wir die Logs dieser beiden Systeme nicht
miteinander ab.  Wir haben auch keine Infrastruktur, um das zu tun.

Das Serverlog enthält Ihre IP-Nummer.  Wenn Sie sich der Anwendung
gegenüber registrieren oder anmelden oder Ihre Email-Anschrift
bestätigen, enthält das Anwendungslog personenbezogene Informationen.
Durch Abgleich von Serverlog und Anwendungslog kann in diesem Fall die
Pseudonymisierung/Anonymisierung Ihrer IP-Nummer aufgehoben werden,
die eigentlich anonymisierte IP-Nummer also wieder Ihnen zugeordnet
werden.

Derselben Server ist auch für einige andere Domains zuständig.  Das
betrifft delta25.de sowie einige wenige andere Domains, deren
Zielgruppe sich mit der dieser Webanwendung nicht wesentlich
überschneidet.  Um etwaige Angriffe auf den gemeinsamen Server besser
analysieren zu können, nutzen alle Domains einen identischen
Pseudonymisierungs/Anonymisierungs-Algorithmus für IP-Nummern.

Wenn wir also die Logs der Anwendung abgleichen würden mit denen des
Servers, könnten wir aus den vorhandenen Daten im Prinzip
herausfinden, ob Sie am selben Tag, an dem Sie auf diese Webanwendung
zugegriffen haben, vorher oder nachher auch auf eine oder mehrere
andere dieser wenigen Domains zugegriffen haben, und die einzelnen
Zugriffe Ihnen zuordnen.  Wie gesagt, wir haben für so einen Abgleich
derzeit keine Infrastruktur vorrätig und führen so einen Abgleich auch
nicht durch.

## Loggen von lesenden Zugriffen

Lesende Zugriffe werden im Anwendungslog nicht personenbezogen geloggt.

## Externe Resourcen

Diese Webanwendung bindet keine externen Resourcen ein (keine Fonts
von Google, keine JavaScript von JQuery, keine Buttons von Facebook,
oder ähnliches).  Was wir brauchen, liefern wir selbst aus.

## Ihre Rechte

Sie haben neben den oben schon erwähnten Rechten auch das Recht, sich
bei den zuständigen Stellen zu beschweren.

