<navigation class="b-static-pages_navigation" markdown="block">

# Für Eilige...

Der schnelle Weg zu den beiden Übersichtsseiten:

* [geplanten Aktivitäten](/stations/1/upcoming)
* [bisherigen Aktivitäten](/stations/1/past)

</navigation>

# D wird 70

Der [Distrikt D](https://www.darc.de/der-club/distrikte/d/) des [DARC
e.V.](https://www.darc.de/der-club/allgemeines/) wird demnächst 70
Jahre alt!  Vorläuferorganisationen hatte es schon eher gegeben.  Aber
so weit wir wissen, wurde der Distrikt selbst im April 1949 gegründet.

## Das Angebot

&ldquo;Amateurfunk&rdquo; ist bekanntlich nicht nur _ein_ Hobby,
sondern bietet _viele_ Hobbys.  Eines dieser vielen Amateurfunkhobbys
beschäftigt sich mit dem Sammeln von Sonder-DOKs.

Der Distrikt D möchte denjenigen seiner Mitglieder, die an so etwas
Spaß haben, ein Angebot machen: Wer mag, kann auf der anderen Seite
stehen und selbst QSOs mit einem Sonder-DOK verteilen.

Die Aktion findet ein Vierteljahr lang statt: Von April bis Juni 2019.

## dr70bln

Wir haben inzwischen das Klubstationsrufzeichen dr70bln von der BNetzA
amtlich zugeteilt bekommen:

<img alt="Die dr70bln Urkunde" src="/images/dr70bln.png" style="width: 50%; padding-left: 10%" />

Diese Klubstation wird viel auf Reisen sein.  Die an der
Aktion Teilnehmenden werden ihr die heimischen Standorte und auch
Geräte zur Verfügung stellen.

## 70D

Und wir haben auch den Sonder-DOK im Kasten (sogar viele Monate länger,
als das Klubstationsrufzeichen selbst gilt):

<img alt="Die 70d SDOK Urkunde" src="/images/70d.png" style="width: 50%; padding-left: 10%" />

## Sonstige Vorbereitungen

Ich habe schon ein LoTW-Zertifikat für DR70BLN, und die Station auch schon
in [hamqth.com](https://www.hamqth.com/dr70bln) eingetragen.

## d-70.de als Reservierungssystem

Weiter gibt es hier auf der Webseite ein
[Reservierungssystem](/stations/1/reservations/new).
Damit können sich teilnehmende Funkamateure um konkrete Zeiträume bewerben:
Stundenweise, ganze Nachmittage oder Abende, oder auch Tage.  Zu
diesen Zeiten können sie dr70bln nutzen und den Sonder-DOK verteilen.

## d-70.de als Log-Drehscheibe, QSL-Karten

Weiter wird diese Webseite als Log-Drehscheibe funktionieren.  Wir
sammeln hier die Logs ein und geben sie an die angemeldeten Aktiven
auch wieder raus.

Das d-70.de-Team kümmert sich darum, dass QSL-Karten rausgehen und die
Logs an DCL und LotW gemeldet werden.

Diesen Teil des Systems gibt es noch nicht.  Aber eins nach dem
anderen...

## Rahmenbedingungen

Wie funktionieren eigentlich Klubstationen und Sonder-DOKs?
Dazu gibt es eine [eigene Seite](Rahmenbedingungen).

## Dies ist kein Hilferuf!

... sondern ein Angebot.  Wir sind gerne bereit, denjenigen von Euch,
denen ein Sonder-DOK Spaß machen würde, eine Freude zu machen.  Wer
noch mitmachen will, meldet sich einfach an.

### Testdaten

Wenn es Probleme mit dieser Seite gibt: Bugreports werden als
hilfreich empfunden, wenn sie so gemeint sind!  Zur Zeit bitte als
Email an mich.  Wer dafür hier herumspielen möchte, bitte sehr: Man
registriert sich ernsthaft, trägt dann aber als seinen DOK D99 ein.
Fragebögen mit DOK D99 werden wir ignorieren, wenn es ernst wird.  Wir
löschen sie dann gelegentlich.  Man kann auch erst eine Weile mit DOK
D99 Quatsch eintragen, den Fragebogen später ernsthaft ausfüllen und
dabei auf seinen wirklichen DOK wechseln.
