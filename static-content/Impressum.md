## Impressum

Diese Webseite wird privat betrieben und verantwortet von     
Dr. Andreas Krüger, Herweghstr. 13, 12487 Berlin, 030 9832 4321, <a href="mailto:dj3ei@d-70.de">dj3ei@d-70.de</a> .

Ich benutze übrigens verschiedene Emailanschriften.  Aber
Emailanschriften sind ohnehin fälschbar.  Wer bezüglich einer
Email-Nachricht überprüfen will, ob sie von mir stammt: Wenn die
Nachricht mit meinem GPG-Key unterschrieben wurde, ist sie sicherlich
von mir persönlich geschrieben worden.

Den Key man sich mit Hilfe seines Fingerabdrucks besorgen wie folgt:

    gpg --receive-keys 0xADE253DD83C9B8B1CB5B381D4779172E114322B8

Ich unterschreibe fast alle Nachrichten, die ich schicke.

Nachrichten, die von der Software hinter d-70.de automatisch erzeugt
werden, sind dagegen nicht unterschrieben.
