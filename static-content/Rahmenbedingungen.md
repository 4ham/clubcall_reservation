# Rahmenbedingungen

Wir bewegen uns mit unserer Aktion in einem Rahmen, der von
verschiedenen Regelwerken vorgegeben ist.  Hier das Wichtigste
zusammengefasst, jeweils mit Links auf die Original-Regelwerke.

## Klubstationen

Ein Rufzeichen darf normalerweise zu jeder Zeit nur an _einem_
Standort genutzt werden. Dazu gibt es eine
[Amtsblattverfügung](https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Telekommunikation/Unternehmen_Institutionen/Frequenzen/Amateurfunk/AmtsblattverfuegungenAFu/Vfg132005Einzelheitenf2Id1318pdf.pdf)
der BNetzA.

Man kann Ausnahmegenehmigungen beantragen, aber das haben wir nicht
vor.

Dagegen interessiert sich die BNetzA nicht dafür, ob ein vergebenes
Rufzeichen am zugeordneten ortsfesten Standort betrieben wird oder
kurzfristig woanders.  (Für diese Tatsache habe ich keine Quelle, aber
das "Wandern" von Klubstationen ist übliche, gelebte Praxis.)

## Sonder-DOKs

[Sonder-DOKs](https://www.darc.de/funkbetrieb/sonder-doks/)
dürfen nur für Klubstationsrufzeichen beantragt werden und können nur
von Klubstationen vergeben werden.

Der gleiche Sonder-DOK kann an mehrere Rufzeichen aus
unterschiedlichen Ortsverbänden zugewiesen werden.  Das steht wörtlich
so in der
Sonder-DOK-[Nutzungsinformation](https://www.darc.de/funkbetrieb/sonder-doks/#c120630).
Nach den [Daten](https://www.darc.de/funkbetrieb/sonder-doks/#c120623)
ist gleichzeitige Sonder-DOK-Vergabe an mehrere Klubstationen auch
durchaus üblich. Nach interner Abstimmung hat das dr70bln-Team entschieden,
nur mit einem Klubstationsrufzeichen zu arbeiten.

### Logs

Die Logs von Sonder-DOKs müssen ins
[DCL](https://dcl.darc.de/~dcl/public/index.php) hochgeladen werden.
Wir wollen sie außerdem über das [LotW](https://lotw.arrl.org/) zur
Verfügung stellen. Und Papier-QSL-Karten sind natürlich Ehrensache!

