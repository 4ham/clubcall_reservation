# coding: utf-8
class ReservationsController < ApplicationController
  before_action :set_station, only: [:create]
  before_action :set_admin_user
  before_action :authenticate_user!, except: [:index, :show]
  before_action :ensure_validated_user!, except: [:index, :show, :new]
  before_action :set_r, only: [:show, :edit, :update]
  before_action :set_may_change, only: [:show, :edit, :update]
  before_action :ensure_may_change!, only: [:update]
  before_action :set_may_log, only: [:show]
  before_action :list_next_event_types, only: [:edit, :update]

  def index
  end

  def new
    @page = "reservation.new"
    @validated_user = validated_user?
    @station = Station.find(params[:station_id])
    @reservation = Reservation.new
    next_week = Time.now.getutc + 7.days
    @reservation.station = @station
    @reservation.user = current_user
    @reservation.start_time = Time.new(next_week.year, next_week.month, next_week.day, next_week.hour, 0, 0, "+00:00")
    @reservation.end_time = @reservation.start_time + 7200
    def @reservation.op_publish
      user.user_preference && user.user_preference.default_op_publish
    end
    def @reservation.importance
      "normal"
    end
    def @reservation.decision_wanted
      s = (start_time - 4.days).getutc
      Time.new(s.year, s.month, s.day, 0, 0, 0, "+00:00")
    end
    def @reservation.qrg_mode
      "CW auf Kurzwelle"
    end
  end

  def create
    @validated_user = validated_user?
    rp = params[:reservation]
    @reservation = Reservation.make(
      current_user,
      @station,
      parse_time(rp[:start_time]),
      parse_time(rp[:end_time]),
      rp[:importance],
      rp[:op_publish].nil? ? false : rp[:op_publish] == "true",
      rp[:comment].present? ? rp[:comment] : nil,
      parse_time(rp[:decision_wanted]),
      rp[:qrg_mode].present? ? rp[:qrg_mode] : nil
    )
    @now = Time.now.getutc
    if @reservation.start_time <= @now
      @reservation.errors.add(:start_time,
                              "Reservierungen für die Vergangenheit sind zu aufwändig " +
                              "wegen der nötigen Zeitreisen." +
                              " (Notfalls Beicht-Nachricht an dj3ei schreiben.)")
      render :new
    else
      if @reservation.save
        if @reservation.start_time - Time.now.getutc < 3.days && @reservation.auto_accept_maybe
          redirect_to @reservation, \
          notice: "Folgende Reservierungsanfrage wurde im Schnellverfahren genehmigt:"
        else
          redirect_to @reservation, notice: "Folgende Reservierungsanfrage ist gespeichert:"
        end
      else
        render :new
      end
    end
  end

  def show
    @overlapping = Reservation.find_overlapping(@r.station_id, @r.start_time, @r.end_time).find_all{|r| r.id != @r.id}
  end

  def edit
    unless @may_change
      redirect_to reservation_path(@r)
    end
  end

  def update
    rp = params[:reservation]
    event_type = rp[:event_type].nil? ? @r.present_state : rp[:event_type]
    decision_wanted = parse_time(rp[:decision_wanted])
    pe = @r.last_event
    op_pub_in = rp[:op_publish]
    @r.add_event(event_type,
                 @admin_user,
                 nil_unless_changed(rp[:importance], @r.importance.to_s),
                 op_pub_in.nil? ? nil : nil_unless_changed(rp[:op_publish] == "true", @r.op_publish),
                 nil_unless_changed(rp[:comment], @r.comment),
                 nil_unless_changed(decision_wanted, @r.decision_wanted),
                 nil_unless_changed(rp[:qrg_mode], @r.qrg_mode))
    if @r.save
      redirect_to reservation_path(@r), notice: "Änderung ist gespeichert."
    else
      render :edit
    end
  end

  protected

  def nil_unless_changed(new, old)
    if new.nil? || new == old
      nil
    else
      logger.info("Changing for #{@r.id}: #{old} -> #{new}")
      new
    end
  end

  def set_r
    @r = Reservation.find_by_id_full(params[:id])
    @station = @r.station
  end

  def set_may_change
    @may_change = ((current_user && (current_user.id == @r.user.id)) || admin_user?) && @r.edit_makes_sense?
  end

  def set_may_log
    @may_log = ((current_user && (current_user == @r.user)) || admin_user?) && @r.expects_log?
  end

  def ensure_may_change!
    unless @may_change
      who = current_user ? current_user.callsign : "anonymous"
      logger.error("#{who} attempts to edit reservation #{@r.id}")
      raise NotOwnResources.new("#{who} attempts to edit reservation #{@r.id}")
    end
  end

  def list_next_event_types
    @next_event_types = @r.ui_choices(admin_user?)
  end

  def parse_time(hs)
    if hs
      y = hs[:year]
      mo = hs[:month]
      d = hs[:day]
      h = hs[:hour]
      mi = hs[:minute]
      if y.present? && mo.present? && d.present? && h.present?
        y = y.to_i
        mo = mo.to_i
        d = d.to_i
        h = h.to_i
        mi = mi.present? ? mi.to_i : 0
        Time.new(y, mo, d, h, mi, 0, "+00:00")
      else
        nil
      end
    else
      nil
    end
  end
end
