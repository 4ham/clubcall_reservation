# coding: utf-8
class StatusController < ApplicationController
  before_action :authenticate_user!, except: [:upcoming, :past]
  before_action :ensure_admin_user!, only: [:admin]
  before_action :set_station, except: [:admin]
  before_action :set_now

  QUICK_REFRESH = 60.seconds
  SLOW_REFRESH = 10.minutes

  def admin
    @users_to_confirm = User.includes(:user_preference).
                        joins(:user_preference).
                        where("users.verified_member = false and user_preferences.log_provide_adif = true").
                        order(:callsign)
    @stations = Station.all.order(:id).to_a
    @station2grant_wait = {}
    @stations.each do |station|
      waiting_ids = Reservation.joins(:reservation_events).
                where("reservation_events.station_id = :my_station_id and " +
                      "reservation_events.last_for_reservation = true and " +
                      "reservation_events.event_type = :my_requested and " + 
                      "reservations.station_id = :my_station_id",
                      {my_station_id: station.id,  my_requested: ReservationEvent.event_types[:requested]}).
                all.to_a.map{|r| r.id}
      waiting = Reservation.includes(:reservation_events, :station, :user).
                where("id in (:ids)",  {ids: waiting_ids}).
                all.to_a.
                sort do |r1, r2|
                  dw = (r1.decision_wanted <=> r2.decision_wanted)
                  dw == 0 ? (r1.start_time <=> r2.start_time) : dw
                end
      possibly_conflicting = Reservation.find_potentially_conflicting_reservations(waiting)
      @station2grant_wait[station.id] = waiting.find_all do |res|
        res.filter_worst_conflict(possibly_conflicting) != :granted
      end
    end
  end

  def op
    @statuspage = true
    if params[:user_id] && admin_user?
      @user = User.find(params[:user_id].to_i)
    else
      @user = current_user
    end
    @need_preference = @user.user_preference.nil?
    @need_log_promise = @need_preference || not(@user.user_preference.log_provide_adif)
    @wait_for_verify = not(@user.verified_member)
    if @need_preference || @need_log_promise || @wait_for_verify
      @about_to_activate =
        @past_activations =
        @wait_for_logs =
        @wait_for_grants =
        @not_granted =
        @canceled = []
    else
      @about_to_activate, @past_activations = Reservation.activations(@station.id, @user.id, @now)
      @canceled = Reservation.canceled(@station.id, @user.id)
      @wait_for_logs = Reservation.wait_for_log(@station.id, @user.id, @now)
      @wait_for_grants, @not_granted = Reservation.wait_for_grants(@station.id, @user.id, @now)
      @nothing_yet = not(@about_to_activate.any? || @past_activations.any? || @wait_for_logs.any? ||
                         @wait_for_grants.any? || @not_granted.any? || @canceled.any?)
    end
  end

  def past
    res_ids = ReservationEvent.
              where("reservation_events.station_id = :my_station_id and " +
                    "reservation_events.last_for_reservation = true and " +
                    "(" +
                    "reservation_events.event_type = :my_granted or " +
                    "reservation_events.event_type = :my_terminated or " +
                    ":my_log_received <= reservation_events.event_type" +
                    ")",
                    {
                      my_station_id: @station.id,
                      my_granted: ReservationEvent.event_types[:granted],
                      my_terminated: ReservationEvent.event_types[:terminated],
                      my_log_received: ReservationEvent.event_types[:log_received]
                    }).
              to_a.map{|rev| rev.reservation_id}
    @reservations = Reservation.includes(:user, :reservation_events).
                    where("id in (:my_ids) and start_time <= :now",
                          {my_ids: res_ids, now: @now}).
                    order(start_time: :desc).to_a
  end

  def upcoming
    if params[:refresh] != "true" && params[:refresh] != "false"
      redirect_to station_upcoming_path @station.id, refresh: "true"
    else
      refresh = params[:refresh] == "true"
      if params[:commit] || params[:utf8]
        # Reduce tail ?utf8=✓&refresh=false&commit=abschalten to ?refresh=false
        redirect_to station_upcoming_path @station.id, refresh: refresh
      else
        # Presently, this has ALL granted and requested events.
        # We rely on that later.
        res_ids = ReservationEvent.
          where("reservation_events.station_id = :my_station_id and " +
                "reservation_events.last_for_reservation = true and " +
                "(reservation_events.event_type = :my_granted or " +
                "reservation_events.event_type = :my_requested)",
                {my_station_id: @station.id,
                 my_requested: ReservationEvent.event_types[:requested],
                 my_granted: ReservationEvent.event_types[:granted]}).
          to_a.map{|rev| rev.reservation_id}
        
        wanted = Reservation.includes(:user, :reservation_events).
                    where("id in (:my_ids) and " +
                          ":my_now <= reservations.end_time and " + 
                          "reservations.station_id = :my_station_id",
                          {my_ids: res_ids,
                           my_now: @now,
                           my_station_id: @station.id}).
                    order(:start_time, :end_time).
                    to_a
        @upcoming = wanted.find_all {|res| :granted != res.filter_worst_conflict(wanted)}
        if refresh
          if @upcoming.empty?
            @status_autorefresh = SLOW_REFRESH
          else
            @first = @upcoming.first
            if @first.start_time <= @now
              @presently_active = true
              @status_autorefresh = QUICK_REFRESH
            else
              @status_autorefresh = (@first.start_time - @now + 1).to_i
              if @status_autorefresh < QUICK_REFRESH
                @status_autorefresh = QUICK_REFRESH
              elsif SLOW_REFRESH < @status_autorefresh
                @status_autorefresh = SLOW_REFRESH
              end
            end
          end
        end
      end
    end
  end

  protected

  def set_now
    @now = Time.now.getutc
  end
  
end
