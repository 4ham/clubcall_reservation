# coding: utf-8
require 'set'

class EmailsController < ApplicationController
  before_action :ensure_admin_user!

  # GET /emails
  def index
    uids = Set.new
    UserPreference.where(anniversary_mail: true).find_each do |up|
      uids.add up.user_id
    end
    @emails = SortedSet.new
    User.find_each do |u|
      if uids.include? u.id
        @emails.add("#{u.callsign.upcase} <#{u.email}>")
      end
    end
  end
end
