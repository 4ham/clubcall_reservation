# coding: utf-8
# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController

  include CaptchaProtected
  
  before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    set_minimum_password_length
    @captcha = CaptchaTemplate.give_captcha_to_ask
    super
  end

  # POST /resource
  def create
    if check_captcha
      begin
        build_resource(sign_up_params)
        resource.save
        if resource.persisted?
          # Confirmation mail should have been sent out
          set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
          expire_data_after_sign_in!
          redirect_to "/Registrierung"
        else
          logger.warn "User konnte nicht gespeichert werden: #{resource.errors.messages}"
          flash[:alert] = resource.errors.messages.each_value.to_a.join("\n")
          clean_up_passwords resource
          set_minimum_password_length
          @captcha = CaptchaTemplate.give_captcha_to_ask
          redirect_to new_user_registration_path
        end
      rescue ActiveRecord::RecordNotUnique
        flash[:alert] = "Rufzeichen und Email müssen eindeutig sein - eins davon haben wir schon!"
        redirect_to new_user_registration_path
      end
    else
      # Captcha not solved
      flash[:alert] = "Sie haben das Rätsel nicht gelöst. (Sind Sie Mensch und Funkamateur?)"
      redirect_to new_user_registration_path
    end
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    pw_to_check = params[:user][:current_password]
    if pw_to_check
      if current_user.valid_password?(pw_to_check)
        user = User.find(current_user.id)
        user.password = params[:user][:password]
        user.password_confirmation = params[:user][:password_confirmation]
        if user.save
          signed_out = sign_out
          flash[:notice] = "Password geändert. Mit dem neuen geht's weiter."
          redirect_to new_user_session_path
        else
          flash[:alert] = user.errors.messages.each_value.to_a.join("\n")
          redirect_to edit_user_registration_path
        end
      else
        flash[:alert] = "Altes Password ungültig."
        redirect_to edit_user_registration_path
      end
    else
      flash[:alert] = "Altes Password fehlt."
      redirect_to edit_user_registration_path
    end
  end

  # DELETE /resource
  def destroy
    flash[:alert] = "Account wegwerfen haben wir nicht implementiert."
    redirect_to "/vergessen"
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:callsign, :email])
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  def after_inactive_sign_up_path_for(resource)
    return new_user_session_path
  end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  def account_update_params
    # The devise stuff is too indirect for my taste.
    # Make sure this is all the parameters we allow to come in for update.
    # No fiddling of emails for now.
    ["password", "password_confirmation"].reduce({}) do |h, k|
      h[k] = params["user"][k]
      h
    end
  end
end
