# coding: utf-8
class UserAdminController < ApplicationController
  before_action :ensure_admin_user!
  
  def index
    user = User.includes(:user_preference).all.to_a
    by_v = {true => [], false => []}
    user.each {|u| by_v[u.verified_member] << u}
    @by_verified = {true => split_by_ov(by_v[true]), false => split_by_ov(by_v[false])}
  end

  def update
    id = params[:id]
    ver = params[:user][:verified] == "true"
    u = User.find(id)
    u.verified_member = ver
    if u.save
      notice = "User #{u.callsign} verifiziertes Mitglied: #{ver} (Mail geht raus)."
      UserVerifiedMailer.with(user_id: u.id).you_are_on_mail.deliver_later
    else
      notice = "Da hat was nicht geklappt."
    end
    redirect_to user_admin_index_path, notice: notice
  end

  protected

  def split_by_ov(u_a)
    result = {}
    u_a.each do |u|
      ov = u.user_preference.nil? ? "NONE" :
             (u.user_preference.ov.present? ? u.user_preference.ov : "NONE")
      if result.has_key? ov
        result[ov] << u
      else
        result[ov] = [u]
      end
    end
    result.each_key do |ov|
      u_of_ov = result[ov]
      result[ov] = u_of_ov.sort{|u1, u2| u1.callsign <=> u2.callsign}
    end
    result
  end
        
end
