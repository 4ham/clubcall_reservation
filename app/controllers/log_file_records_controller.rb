# coding: utf-8
class LogFileRecordsController < ApplicationController
  before_action :set_reservation, only: [:new, :create, :index]
  before_action :retrieve_reservation_and_lfr, only: [:edit, :show, :destroy]
  before_action :ensure_validated_user!
  before_action :user_may_access
  before_action :set_now
  before_action :reservation_expects_log!, only: [:new, :create]

  class LogNotExpected < RuntimeError
  end

  rescue_from LogNotExpected, with: :log_not_expected

  def new
    @lfr = @reservation.log_file_records.build()

    # Reasonable default values
    @lfr.log_contains_irrelevant_qsos = false
    @lfr.callsign = @reservation.station.name
    @lfr.satellite = :noot_one
    @lfr.packet_radio = :noot_one
    @lfr.internet_assisted = :noot_one
    @lfr.repeater = :noot_one
    @lfr.one_qth = :each_one
    @lfr.mobile = :noot_one
    @lfr.qsl_paper = :no_info
    @lfr.land_station = :each_one
  end

  def create
    @lfr = @reservation.log_file_records.build()
    @lfr.update(lfr_params)
    if @lfr.save
      redirect_to reservation_path(@reservation), notice: "Dieses Log ist gespeichert. Noch ein weiteres für diese Aktivierung?"
    else
      @lfr.adif_log.purge if @lfr.adif_log
      render :new
    end
  end

  def show
    render :edit
  end

  def edit
  end

  def destroy
    if @lfr.may_be_changed?
      if @lfr.destroy
        redirect_to reservation_path(@reservation), notice: "Dieses Log ist gelöscht worden."
      else
        redirect_to reservation_path(@reservation), notice: "Dieses Log konnte nicht gelöscht werden."
      end
    else
      redirect_to log_file_record_path(@lfr), notice: "Bitte ganz schnell Mail! - Dieses Log kann nicht mehr gelöscht worden, da schon (teilweise) verarbeitet."
    end
  end

  private
  def set_reservation
    @reservation = Reservation.find_by_id_full(params[:reservation_id])    
  end

  def retrieve_reservation_and_lfr
    @lfr = LogFileRecord.includes(:reservation).find(params[:id])
    @reservation = @lfr.reservation
  end

  def user_may_access
    unless @reservation.user == @current_user or admin_user?
      raise NotOwnResources.new("#{@current_user.callsign} may not access log files for reservation #{@reservation.id}")
    end
  end

  def reservation_expects_log!
    unless @reservation.expects_log?(@now)
      raise LogNotExpected.new("Reservation #{@reservation.id} is not expecting a log file.")
    end
  end

  def log_not_expected
    render plain: "Zu Reservierung #{@reservation.id} können (derzeit?) keine Log-Dateien abgegeben oder gelöscht werden." +
           " (Wenden Sie sich ggf. an den Admin.)", status: 409
  end

  def set_now
    @now = Time.now.getutc
  end

  def lfr_params
    params.require(:log_file_record).
      permit(:adif_log, :callsign, :locator, :land_station, :one_qth, :repeater, :satellite, :satellite_name,
             :packet_radio, :internet_assisted, :mobile, :log_contains_irrelevant_qsos, :qsl_paper,
             :dlff, :gma)
  end
end
