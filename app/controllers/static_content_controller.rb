# coding: utf-8
class StaticContentController < ApplicationController

  def show
    begin
      slug = params[:slug]
      if not(slug)
        slug = "ROOT"
      end
      if slug == "Rufzeichenüberprüfung"
        authenticate_user!
      end
      @content = StaticContent.find(slug)
      @datenschutz = slug == "Datenschutzerklärung"
      @impressum = slug == "Impressum"
      @lizenz = slug == "Lizenz"
      fresh_when(strong_etag: @content, last_modified: @content.last_modified, public: not(current_user))
    end
  rescue Errno::ENOENT
    Rails.logger.warn "Handing out 404 for #{slug}"
    render file: Rails.root / "public" / "404.html", layout: false, status: :not_found
  end
end
