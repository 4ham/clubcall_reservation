# coding: utf-8
class ApplicationController < ActionController::Base
  before_action :get_hostname

  class NoAdminUser < RuntimeError
  end

  class NotOwnResources < RuntimeError
  end

  class NotYetValidatedUser < RuntimeError
  end

  before_action :cache_fiddle

  before_action :list_user
  before_action :grab_stations

  rescue_from NoAdminUser, with: :not_admin_user
  rescue_from NotOwnResources, with: :not_own_resources
  rescue_from NotYetValidatedUser, with: :not_yet_validated_user

  protected

  def get_hostname
    @hostname = request.host
  end

  def cache_fiddle
    expires_in 1.hour, public: false, must_revalidate: true
  end

  def list_user
    if current_user && not(request.get? || request.head?)
      Rails.logger.info "Doing stuff as user \"#{current_user.callsign}\"."
    end
  end

  def grab_stations
    @stations = Station.all.to_a
  end
  
  def admin_user?
    current_user && current_user.is_admin && current_user.verified_member
  end

  def set_admin_user
    @admin_user = admin_user?
  end

  def validated_user?
    current_user && (current_user.verified_member || admin_user?)
  end

  def ensure_admin_user!
    # We cancancan do something more sophisticated here, but for now:
    unless admin_user?
      if current_user
        Rails.logger.warn("User \"#{current_user.callsign}\" tried to do admin stuff.")
      else
        Rails.logger.warn("Not-logged-in person tried to do admin stuff.")
      end
      raise NoAdminUser.new("#{current_user} is no admin")
    end
    Rails.logger.info("Doing stuff as admin #{current_user.callsign}")
  end

  def ensure_validated_user!
    unless validated_user?
      Rails.logger.warn("User #{current_user.callsign} needs to be validated.") if current_user
      raise NotYetValidatedUser.new("#{current_user} has not been validated yet.")
    end
    @current_user = current_user
  end
  
  def not_yet_validated_user
    render plain: "Wir sind mit Ihrer Benutzerkennung leider noch nicht so weit.\n\n" +
           "Ehe Sie hier richtig mitmachen können,\n" +
           "müssen Ihre Kombination Rufzeichen / Email\n" +
           "und Ihre Zugehörigkeit zum Distrikt D\n" +
           "noch (manuell) überprüft werden.", status: 401
  end

  def not_admin_user
    render plain: "Das sind nicht Ihre Daten und daher vor Ihnen geschützt.", status: 401
  end

  def not_own_resources
    render plain: "Das sind nicht Ihre Daten und daher vor Ihnen geschützt.", status: 401
  end

  def set_station
    @station = Station.find(params[:station_id])
  end
end
