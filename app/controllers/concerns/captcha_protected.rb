# coding: utf-8
module CaptchaProtected
  extend ActiveSupport::Concern

  included do
    
  end

  def check_captcha
    id, answer = params[:captcha_id], params[:captcha_answer]
    if id && answer && CaptchaTemplate.check_answer(id, answer)
      return true
    else
      flash[:alert] = "Sind Sie ein Funkamateur? Bitte das Rätsel lösen!"
      return false
    end
  end
end
