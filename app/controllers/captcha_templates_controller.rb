class CaptchaTemplatesController < ApplicationController
  before_action :authenticate_user!
  before_action :ensure_admin_user!
  before_action :set_captcha_template, only: [:show, :edit, :update, :destroy]

  # GET /captcha_templates
  # GET /captcha_templates.json
  def index
    @captcha_templates = CaptchaTemplate.all
  end

  # GET /captcha_templates/1
  # GET /captcha_templates/1.json
  def show
  end

  # GET /captcha_templates/new
  def new
    @captcha_template = CaptchaTemplate.new
  end

  # GET /captcha_templates/1/edit
  def edit
  end

  # POST /captcha_templates
  # POST /captcha_templates.json
  def create
    @captcha_template = CaptchaTemplate.new(captcha_template_params)

    respond_to do |format|
      if @captcha_template.save
        format.html { redirect_to @captcha_template, notice: 'Captcha template was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /captcha_templates/1
  # PATCH/PUT /captcha_templates/1.json
  def update
    respond_to do |format|
      if @captcha_template.update(captcha_template_params)
        format.html { redirect_to @captcha_template, notice: 'Captcha template was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /captcha_templates/1
  # DELETE /captcha_templates/1.json
  def destroy
    info = if @captcha_template.invalidate
             {notice: 'Captcha template was successfully invalidated.'}
           end
    @captcha_template.errors.add(:can_be_asked, 'Just testing errors')
    respond_to do |format|
      format.html {
        redirect_to captcha_templates_url, info
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_captcha_template
      @captcha_template = CaptchaTemplate.find(params[:id])
    end

    def captcha_template_params
      params.require(:captcha_template).permit(:question, :solution_regexp)
    end
end
