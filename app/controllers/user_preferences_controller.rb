# coding: utf-8
class UserPreferencesController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  # before_action :ensure_admin_user!, only: [:index]
  before_action :set_user_preference, only: [:show, :edit, :update, :destroy]
  before_action :check_own_preferences, only: [:show, :edit, :update, :destroy]

  # GET /user_preferences
  def index
    @user_preferences = UserPreference.all.select{|up| "D99" != up.ov}

    @ov2count = @user_preferences.reduce({}) do |h, up|
      ov = up.ov
      if h.has_key? ov
        h[ov] += 1
      else
        h[ov] = 1
      end
      h
    end
    @count_old_questionaire = @user_preferences.select{|up| up.older_than_question_version?}.count
    @count_member_d = @user_preferences.select{|up| up.member_d}.count
    @count_log_provide_adif = @user_preferences.select{|up| up.log_provide_adif}.count
    @total_april_hours = @user_preferences.select{|up| up.pledge_hours_april && 0 < up.pledge_hours_april} \
                         .map{|up| up.pledge_hours_april}.reduce(0, :+)
    @total_may_hours = @user_preferences.select{|up| up.pledge_hours_may && 0 < up.pledge_hours_may} \
                         .map{|up| up.pledge_hours_may}.reduce(0, :+)
    @total_june_hours = @user_preferences.select{|up| up.pledge_hours_june && 0 < up.pledge_hours_june} \
                         .map{|up| up.pledge_hours_june}.reduce(0, :+)

    @admin_user = admin_user?

    @fragebogen_ergebnisseite = true
  end

  # GET /user_preferences/1
  def show
  end

  # GET /user_preferences/new
  def new
    if current_user.user_preference.nil?
      @user = current_user
      @user_preference = UserPreference.new
      @user_preference_page = true
    else
      redirect_to edit_user_preference_path(current_user.user_preference)
    end
  end

  # GET /user_preferences/1/edit
  def edit
    if @user_preference.nil?
      redirect_to new_user_preference_path
    end
  end

  # POST /user_preferences
  def create
    @user = current_user
    @user_preference = UserPreference.new(user_preference_params)
    @user_preference.user_id = current_user.id
    @user_preference.question_version = @user_preference.current_question_version
    puts @user_preference.inspect
    if @user_preference.save
      redirect_to @user_preference, notice: 'Danke für Ihre Antwort! Sie ist gespeichert.'
    else
      render :new
    end
  end

  # PATCH/PUT /user_preferences/1
  def update
    @user_preference.question_version = @user_preference.current_question_version
    if @user_preference.update(user_preference_params)
      redirect_to @user_preference, notice: 'Danke für die Änderung! Sie ist gespeichert.'
    else
      render :edit
    end
  end

  # DELETE /user_preferences/1
  def destroy
    @user_preference.destroy
    redirect_to user_preferences_url, notice: 'Schade... Ihre Antworten wurden gelöscht.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_preference
      @user_preference_page = true
      if admin_user? && params[:id]
        @user = User.find(params[:id].to_i)
        @user_preference = @user.user_preference
      else
        @user = current_user
        @user_preference = current_user.user_preference
      end
    end

    # Only allow a trusted parameter "white list" through.
    def user_preference_params
      if current_user.has_reservations?
        result = params.require(:user_preference).
                 permit(:anniversary_mail, :general_mail, :member_d, :ov,
                        :default_op_publish,
                        :help_qslcard, :help_programming, :pledge_hours_april, :pledge_hours_may, :pledge_hours_june)
      else
        result = params.require(:user_preference).
                 permit(:anniversary_mail, :general_mail, :member_d, :ov, :log_provide_adif,
                        :default_op_publish,
                        :help_qslcard, :help_programming, :pledge_hours_april, :pledge_hours_may, :pledge_hours_june)
      end
      result
    end

    def check_own_preferences
      if @user_preference.user_id != current_user.id && not(admin_user?)
        raise NotOwnResources
      end
    end
end
