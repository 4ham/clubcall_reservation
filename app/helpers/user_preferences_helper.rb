# coding: utf-8
module UserPreferencesHelper

  def up_radio_button_tag(up, field, value, options = {})
    field_value = up.public_send(field)
    checked = (field_value == true && value == "true") || \
              (field_value == false && value == "false") || \
              (field_value.nil? && value == "nil")
    radio_button_tag("user_preference[#{field}]", value, checked = checked, options)
  end
  
end
