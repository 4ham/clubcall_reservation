module LogFileRecordsHelper
  def lfr_radio_button_tag(lfr, field, value, options = {})
    field_value = lfr.public_send(field)
    checked = (field_value == true && value == "true") || \
              (field_value == false && value == "false") || \
              (field_value.nil? && value == "nil") || \
              (field_value == value)
    radio_button_tag("log_file_record[#{field}]", value, checked = checked, options)
  end
end
