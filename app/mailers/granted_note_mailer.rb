# coding: utf-8
class GrantedNoteMailer < ApplicationMailer
  def granted_note_email
    @res = Reservation.find_by_id_full params[:reservation_id].to_i
    if may_mail? @res.user
      station = @res.station
      to_mail = @res.user.email
      body = "BEGIN:VCALENDAR\r\n" +
             "PRODID:-//dr70bln-soft/NONSGML by_dj3ei//EN\r\n" +
             "VERSION:2.0\r\n" +
             "CALSCALE:GREGORIAN\r\n" +
             "METHOD:REQUEST\r\n" +
             "BEGIN:VEVENT\r\n" +
             "ORGANIZER;CN=DJ3EI;mailto:dj3ei@d-70.de\r\n" + 
             "UID:#{@res.id}-#{station.name}@d-70.de\r\n" +
             "CREATED:#{@res.last_event.created_at.getutc.strftime('%Y%m%dT%H%M%SZ')}\r\n" +
             "LAST-MODIFIED:#{@res.last_event.created_at.getutc.strftime('%Y%m%dT%H%M%SZ')}\r\n" +
             "DTSTAMP:#{@res.last_event.created_at.getutc.strftime('%Y%m%dT%H%M%SZ')}\r\n" +
             "DTSTART:#{@res.start_time.getutc.strftime('%Y%m%dT%H%M%SZ')}\r\n" +
             "DTEND:#{@res.end_time.getutc.strftime('%Y%m%dT%H%M%SZ')}\r\n" +
             "SEQUENCE:0\r\n" +
             "STATUS:CONFIRMED\r\n" +
             "SUMMARY:Nutzung #{station.name} von \r\n" +
             " #{@res.start_time.getutc} bis \r\n" +
             " #{@res.end_time.getutc}\r\n" +
             "DESCRIPTION:Nutzung von #{station.name} durch \r\n" +
             " #{@res.user.callsign} ist genehmigt.\\n\r\n" +
             " Siehe https://www.d-70.de/reservations/#{@res.id} .\\n\r\n" +
             " Termin annehmen oder ablehnen egal\\n(wird nicht ausgewertet),\\n\r\n" +
             " es zählt www.d-70.de.\r\n" +
             "TRANSP:OPAQUE\r\n" +
             "END:VEVENT\r\n" +
             "END:VCALENDAR\r\n"
      mail(to: to_mail,
           subject: "#{@res.station.name}: Nutzung #{@res.start_time} - #{@res.end_time}",
           content_type: "text/calendar; method=REQUEST; charset=UTF-8",
           body: body)
    end
  end
end
