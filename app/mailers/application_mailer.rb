class ApplicationMailer < ActionMailer::Base
  default from: 'dj3ei@d-70.de'
  layout 'mailer'

  protected
  def may_mail?(user)
    result = user && user.user_preference && user.user_preference.anniversary_mail
    unless result
      ActiveJob::Base.logger.info("#{user.callsign} does not want a mail.");
    end
    result
  end
end
