# coding: utf-8
class UserVerifiedMailer < ApplicationMailer
  def you_are_on_mail
    @user = User.find params[:user_id]
    if may_mail? @user
      mail(to: @user.email, subject: "Der Zugang für #{@user.callsign} zu www.d-70.de ist freigeschaltet.")
    end
  end
end
