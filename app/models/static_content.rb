require 'kramdown'

# A very down-to-earth model that does Markdown processing
# and caching in RAM (for now).

class StaticContent

  attr_accessor(:html, :last_modified)

  @@slug2content_mutex = Mutex.new

  @@slug2content = {}

  def self.find(slug)
    full_path = Rails.root / 'static-content' / "#{slug}.md"

    stat = nil
    begin
      # This throws an exception if the file doesn't exist:
      stat = File.stat(full_path)
    ensure
      if not stat
        # Don't waste memory caching stuff that's no longer there.
        # (Maybe not worth the trouble.  In production, files are unlikely to disappear.
        # But then, it's fast and easy, even in production.)
        @@slug2content_mutex.synchronize do 
          @@slug2content.delete(slug)
        end
      end
    end

    # If the file does not exist,
    # File.stat has already thrown an exception "Errno::ENOENT"
    # (which is duely handled by the controller calling us)
    # and we never get this far.

    result = @@slug2content_mutex.synchronize do
      @@slug2content[slug]
    end

    if not(result) || result.last_modified < stat.mtime
      
      # If the file has disappeared since the stat,
      # File.open also throws "Errno::ENOENT":
      File.open(full_path, mode: "r:UTF-8") do |file|

        md = file.read

        result = StaticContent.new
        # result.html = Kramdown::Document.new(md, parse_block_html: true, parse_span_html: true).to_html
        result.html = Kramdown::Document.new(md).to_html
        result.last_modified = stat.mtime

        @@slug2content_mutex.synchronize do
          # Harmless race condition: Since the previous synchronize,
          # another thread could have done the same work
          # and we may be overwriting that other thread's result.
          # Harmless, as the two results are interchangeable.
          @@slug2content[slug] = result
        end
      end
    end
    result
  end

end
