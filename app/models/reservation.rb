# coding: utf-8
class StartsBeforeEndValidator < ActiveModel::Validator
  def validate(res)
    if res.end_time <= res.start_time
      res.errors[:end_time] << "Nutzung sollte nicht aufhören, ehe sie angefangen hat."
    end
  end
end

class ValidateDecisionWantedNotLaterThanStart < ActiveModel::Validator
  def validate(res)
    dw = res.decision_wanted
    if dw && res.start_time < dw
      res.errors[:base] << "Erst erfahren wollen, ob's klappt, nachdem die Nutzung schon angefangen hat? Das ist unsinnig."
    end
  end
end

class WithinStationAvailabilityValidator < ActiveModel::Validator
  def validate(res)
    if res.start_time < res.station.earliest_start
      res.errors[:start_time] << "Die Nutzung kann nicht anfangen ehe #{res.station.name} verfügbar wird, nämlich #{res.station.earliest_start}."
    end
    if res.station.latest_end < res.end_time
      res.errors[:end_time] << "Die Nutzung kann nicht länger dauern als die #{res.station.latest_end} " +
        "endende Verfügbarkeit von #{res.station.name}."
    end
  end
end

class NotTooLongValidator < ActiveModel::Validator
  def validate(res)
    if res.start_time + Reservation::MAX_DURATION < res.end_time
      res.errors[:base] << "Eine Nutzung kann für maximal #{Reservation::MAX_DURATION / 3600.0} Stunden beantragt werden."
    end
  end
end

class ExactlyOneLastEventValidator < ActiveModel::Validator
  def validate(res)
    unless 1 == res.reservation_events.to_a.find_all{|ev| ev.last_for_reservation}.size
      res.errors[:base] << "Interner Fehler: Nutzung #{res.id} hat gleichzeitig zwei letzte Änderungen."
    end
  end
end

class UserProvidesAdifLogValidator < ActiveModel::Validator
  def validate(res)
    unless (res.user.user_preference && res.user.user_preference.log_provide_adif)
      res.errors[:base] << "Reservierungen nur für OPs, die (laut Fragebogen) ADIF-Logs bereitstellen."
    end
  end
end

class NoDoubleAssignmentValidator < ActiveModel::Validator
  def validate(res)
    critical_states = [:granted]
    my_state = res.present_state
    if critical_states.include? my_state
      r =  Reservation.
          find_overlapping(res.station_id, res.start_time, res.end_time).
          find_all {|r| r.id != res.id}.
          find {|r| critical_states.include? r.present_state}
      if r
        res.errors[:base] << "Interner Fehler: Konflikt mit Nutzung #{r.id}. Dies sollte nicht auftreten!"
      end
    end
  end
end

class ReservationStateChangeIllegalException < RuntimeError
  
end

class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :station
  has_many :reservation_events, -> { order "id ASC" }, autosave: true
  has_many :log_file_records, -> {order "id ASC" }, autosave:true

  validates :start_time, :end_time, :user, :station, presence: true
  validates_with StartsBeforeEndValidator
  validates_with WithinStationAvailabilityValidator
  validates_with NotTooLongValidator
  validates_with NoDoubleAssignmentValidator
  validates_with ExactlyOneLastEventValidator
  validates_with ValidateDecisionWantedNotLaterThanStart
  validates_with UserProvidesAdifLogValidator

  MAX_DURATION = 50 * 3600

  CONFLICTING_STATES = [:requested, :granted].freeze

  def self.make(user, station, start_time, end_time, importance, op_publish, comment, decision_wanted, qrg_mode)
    res = user.reservations.build(station: station, start_time: start_time.getutc, end_time: end_time.getutc)
    res.add_event(:requested, false, importance, op_publish, comment, decision_wanted, qrg_mode)
    res
  end

  def add_event(event_type, is_admin, importance, op_publish, comment, decision_wanted, qrg_mode)
    ets = event_type.to_sym
    ev = nil
    lev = self.last_event
    if lev
      if lev.choices_next_state(is_admin, not(log_file_records.empty?)).include?(ets) || lev.event_type == ets.to_s
        ev = self.reservation_events.build(station_id: self.station_id,
                                           reservation: self,
                                           event_type: ets,
                                           decision_wanted: decision_wanted,
                                           op_publish: op_publish,
                                           importance: importance,
                                           comment: comment,
                                           qrg_mode: qrg_mode,
                                           last_for_reservation: true)
        lev.last_for_reservation = false
      else
        raise ReservationStateChangeIllegalException.new("Cannot reach \"#{ets}\" from \"#{self.present_state}\" as is_admin? == #{is_admin}.")
      end
    else
      if ets == :requested
        ev = self.reservation_events.build(station_id: self.station_id,
                                           reservation: self,
                                           event_type: ets,
                                           decision_wanted: decision_wanted,
                                           op_publish: op_publish,
                                           importance: importance,
                                           comment: comment,
                                           qrg_mode: qrg_mode,
                                           last_for_reservation: true)
      else
        raise ReservationStateChangeIllegalException.new("Cannot start with state \"#{ets}\"")
      end
    end
    ev
  end

  def self.find_by_id_full(id)
    Reservation.includes(:user, :station, :reservation_events, :log_file_records).find(id)
  end

  def self.find_overlapping(my_station_id, my_start_time, my_end_time)
    Reservation.includes(:user, :station, :reservation_events).
      where(":my_station_id = station_id and :min_start_time <= start_time and start_time < :my_end_time and (" +
            # Overlap if their start time is in my interval:
            "(:my_start_time <= start_time and start_time < :my_end_time) or " +
            # Overlap if their end time is in my interval:
            "(:my_start_time < end_time and end_time <= :my_end_time) or " + 
            # Overlap if my start time is in their interval:
            "(start_time <= :my_start_time and :my_start_time < end_time)" +
            # No need to check if my end_time is in their interval:
            # If their start time is earlier than mine, my start time is in their interval;
            # or if their start time is later than mine, their start time is in my interval.
            ")", {
              my_station_id: my_station_id,
              min_start_time: my_start_time - Reservation::MAX_DURATION,
              my_start_time: my_start_time,
              my_end_time: my_end_time
            }).
      all.
      to_a.sort do |a, b|
        st = a.start_time <=> b.start_time
        st == 0 ? a.end_time <=> b.end_time : st
      end
  end

  def self.wait_for_log(station_id, user_id, now = Time.now.getutc)
    wait_for_logs_incomplete =
      Reservation.joins(:reservation_events).
      where("reservation_events.station_id = :my_station_id and reservation_events.last_for_reservation = true and " +
            "(reservation_events.event_type = :my_terminated or " +
            "  (reservation_events.event_type = :my_granted and reservations.end_time <= :my_now)) and " + 
            "reservations.user_id = :my_user_id and reservations.station_id = :my_station_id",
            {my_station_id: station_id, my_user_id: user_id, my_now: now,
             my_terminated: ReservationEvent.event_types[:terminated],
             my_granted: ReservationEvent.event_types[:granted]})

    Reservation.includes(:reservation_events).
      where("id in (:res_ids)", {res_ids: wait_for_logs_incomplete.to_a.map{|r| r.id}}).
      order(:start_time, :end_time)
  end

  def self.wait_for_grants(station_id, user_id, now = Time.now.getutc)
    wait_for_grants_incomplete =
      Reservation.
      joins(:reservation_events).
      where("reservation_events.station_id = :my_station_id and reservation_events.last_for_reservation = true and " +
            "reservation_events.event_type = :my_requested and " + 
            "reservations.user_id = :my_user_id and reservations.station_id = :my_station_id",
            {my_station_id: station_id, my_user_id: user_id, my_requested: ReservationEvent.event_types[:requested]})

    wait_for_grants =
      Reservation.includes(:reservation_events).
      where("id in (:res_ids)", {res_ids: wait_for_grants_incomplete.to_a.map{|r| r.id}}).
      to_a.
      sort do |r1, r2|
      dw = r1.decision_wanted <=> r2.decision_wanted
      dw == 0 ? r1.start_time <=> r2.start_time : dw
    end

    wait_for_grants.reduce [[],[]] do |wn, r|
      if r.end_time <= now
        wn[1] << r
      else
        wn[0] << r
      end
      wn
    end
  end

  def self.canceled(station_id, user_id)
    activation_ids =
      Reservation.
      joins(:reservation_events).
      where("reservation_events.station_id = :my_station_id and reservation_events.last_for_reservation = true and " +
            ":my_canceled = reservation_events.event_type and " + 
            "reservations.user_id = :my_user_id and reservations.station_id = :my_station_id",
            {
                my_station_id: station_id,
                my_user_id: user_id,
                my_canceled: ReservationEvent.event_types[:canceled]
            }
           ).to_a.map{|r| r.id}

      Reservation.includes(:reservation_events).
      where("id in (:res_ids)", {res_ids: activation_ids}).
      order(:start_time, :end_time).
      to_a
  end

  def self.activations(station_id, user_id, now = Time.now.getutc)
    activation_ids =
      Reservation.
      joins(:reservation_events).
      where("reservation_events.station_id = :my_station_id and reservation_events.last_for_reservation = true and " +
            ":my_granted <= reservation_events.event_type and " + 
            "reservations.user_id = :my_user_id and reservations.station_id = :my_station_id",
            {
                my_station_id: station_id,
                my_user_id: user_id,
                my_granted: ReservationEvent.event_types[:granted]
            }
           ).to_a.map{|r| r.id}

    activations =
      Reservation.includes(:reservation_events).
      where("id in (:res_ids)", {res_ids: activation_ids}).
      order(:start_time, :end_time)

    activations.to_a.reduce([[],[]]) do |futurepast, r|
      if now < r.end_time && r.present_state == :granted
        futurepast[0] << r
      else
        futurepast[1] << r
      end
      futurepast
    end
  end

  # Make sure we always have the initial event on creation:
  after_create do
    if reservation_events.empty?
      reserveration_events.add_event(:requested, false, :unimportant, nil, start_time.getutc, nil)
    end
  end

  after_save do
    if present_state == :granted
      granted_events = reservation_events.find_all {|re| re.event_type.to_sym == :granted}
      if granted_events.size == 1
        GrantedNoteMailer.with(reservation_id: id).granted_note_email.deliver_later
      end
    end
  end

  def last_event
    reservation_events.to_a.find {|ev| ev.last_for_reservation}
  end

  def present_state
    last_event.event_type.to_sym
  end

  def ui_state(t)
    p = present_state
    if p == :granted && start_time <= t && t <= end_time
      :currently_active
    else
      p
    end
  end

  def comment
      reverse_events.map {|e| e.comment}.find {|c| c}
  end

  def op
    op_publish ? user.callsign : nil
  end

  def importance
    reverse_events.map {|e| e.importance}.find {|i| i}.to_sym
  end

  def decision_wanted
    reverse_events.map{|e| e.decision_wanted}.find {|t| t}
  end

  def qrg_mode
    reverse_events.map {|e| e.qrg_mode}.find {|q| q}
  end

  def op_publish
    reverse_events.map {|e| e.op_publish}.find {|i| not(i.nil?)}
  end

  def importance
    reverse_events.map {|e| e.importance}.find {|i| i}
  end

  def edit_makes_sense?
    ps = present_state
    (CONFLICTING_STATES.include? ps) ||
      (ReservationEvent::STATES_WAITING_FOR_LOGS.include?(ps) && not(log_file_records.empty?))
  end

  def overlaps?(t_min, t_max)
    (start_time <= t_min && t_min < end_time) ||
      (start_time < t_max && t_max <= end_time) ||
      (t_min <= start_time && start_time < t_max)
  end

  def relevant_for_op?(t_min, t_max)
    CONFLICTING_STATES.include?(present_state) && overlaps?(t_min, t_max)
  end
  
  def self.find_potentially_conflicting_reservations(reservations)
    first = reservations.find{|res| CONFLICTING_STATES.include? res.present_state}
    if first
      s, e = reservations.reduce([first.start_time, first.end_time]) do |se, res|
        if CONFLICTING_STATES.include? res.present_state
          s, e = se
          st = res.start_time
          et = res.end_time
          [s < st ? s : st, e < et ? et : e]
        else
          [s, e]
        end
      end
      # To improve efficiency, the database could do the filtering.
      self.find_overlapping(first.station_id, s, e).
        find_all {|res| CONFLICTING_STATES.include? res.present_state}
    else
      []
    end
  end

  # Returns nil or :requested or :granted
  def filter_worst_conflict(reservations)
    s = start_time
    e = end_time
    
    reservations.reduce(nil) do |prev, res|
      if prev == :granted
        :granted
      elsif res.overlaps?(s, e) && res.id != id
        state = res.present_state
        CONFLICTING_STATES.include?(state) ? state : prev
      else
        prev
      end        
    end
  end

  def conflict_grade
    overlapping = Reservation.find_overlapping(station_id, start_time, end_time)
    if overlapping.any? {|r| r.present_state == :granted}
      return :conflict_with_granted
    else
      my_id = id
      other_requested = overlapping.find_all {|r| r.id != my_id && r.present_state == :requested}
      sym2i = ReservationEvent.importances # {"badly_wanted"=>0, "normal"=>1, "unimportant"=>2}
      my_prio = sym2i[importance]
      more_important, equally_important, less_important = other_requested.reduce [[],[],[]] do |by_i, r|
        case sym2i[r.importance] <=> my_prio
        when -1
          by_i[0] << r
        when 0
          by_i[1] << r
        when 1
          by_i[2] << r
        else
          raise "This was not supposed to happen, confused about #{r.importance.inspect} <=> #{importance.inspect}"
        end
        by_i
      end
      return :conflict_with_more_important unless more_important.empty?
      return :conflict_with_equally_important unless equally_important.empty?
      return :conflict_with_less_important unless less_important.empty?
      return :no_conflict
    end
  end

  def could_be_granted_obviously?
      [:no_conflict, :conflict_with_less_important].include? conflict_grade
  end

  def auto_accept_maybe
    if present_state == :requested && could_be_granted_obviously?
      grant
    else
      false
    end
  end

  def grant
    if present_state == :requested
      add_event(:granted, true, nil, nil, nil, nil, nil)
      save
    else
      false
    end
  end

  def expects_log?(now = Time.now.getutc)
    (present_state == :granted && end_time <= now) || present_state == :terminated
  end

  def ui_choices(admin_user, now = Time.now.getutc)
    result = last_event.ui_choices_next_state(admin_user)
    if now <= end_time || not(result.include? :terminated)
      result
    else
      result.find_all{|et| et != :terminated}
    end
  end

  # One log file has been removed.
  def on_log_reset
    previous_comment = comment
    add_event(:terminated, false, nil, nil, "Status zurückgesetzt da (ein) Log wieder gelöscht wurde.", nil, nil)
    add_event(:terminated, false, nil, nil, previous_comment, nil, nil)
    save!
  end

  protected
  def reverse_events
    reservation_events.
      to_a.
      sort do |a, b|
        bid = b.id
        # Replacing nil with the maximal bigint value, taken from
        # https://www.postgresql.org/docs/9.6/datatype-numeric.html#DATATYPE-INT
        # so nil looks very young indeed.
        bid = bid.nil? ? 9223372036854775807 : bid
        aid = a.id
        aid = aid.nil? ? 9223372036854775807 : aid
        bid <=> aid
    end
  end
end
