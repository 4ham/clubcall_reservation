# coding: utf-8

class UserPreference < ApplicationRecord

  validates :anniversary_mail, :general_mail, :member_d, :log_provide_adif, :inclusion => {in: [true, false]}
  validates :question_version, presence: true
  validates :ov, format: { with: /\A([a-z]\d{1,2})?\Z/i, message: "Kein OV." }

  before_validation :normalize_names

  def current_question_version
    "2019-03-31"
  end

  def older_than_question_version?(date = current_question_version)
    question_version && question_version < date
  end

  private

  def normalize_names
    self.ov = ov.upcase unless ov.blank?
  end
end
