class HasFileValidator < ActiveModel::Validator
  def validate(lfr)
    if lfr.adif_log.attached?
      # "<QSO_DATE:8>20190408<TIME_ON:4>1905<CALL:3>W0N<MODE:2>FM<BAND:2>2M<EOR>" is 71 chars
      # and it does not even have a rapport.
      if lfr.adif_log.blob.byte_size < 71
        if lfr.adif_log.blob.byte_size == 0
          lfr.errors[:adif_log] << "Datei ist leer."
        else
          lfr.errors[:adif_log] << "Datei ist zu kurz, um ADIF zu sein."
        end
      end
    else
      lfr.errors[:adif_log] << "Keine Datei hochgeladen."
    end
  end
end

class LogFileRecord < ApplicationRecord
  belongs_to :reservation
  has_one_attached :adif_log

  validates :callsign, :land_station, :one_qth, :repeater, :satellite, :packet_radio,
            :internet_assisted, :mobile, :reservation_id,
            :qsl_paper, presence: true

  validates_with HasFileValidator

  validates :log_contains_irrelevant_qsos, inclusion: {in: [false, true]}

  after_create :set_blob_content_type

  before_validation :upcase_ota

  before_destroy :destroy_blob
  before_destroy :fiddle_reservation_on_destroy

  NONE_ALL_SOME = {noot_one: 0, each_one: 1, some: 2}.freeze
  NONE_ALL_SOME_ADIF = {noot_one: 0, each_one: 1, some: 2, adif: 3}.freeze
  # "none" and "all" and "each" were already taken by ActiveRecord.
  # "not_one" was greeted with a complaint:
  #  > An enum element in LogFileRecord uses the prefix 'not_'. This will cause a conflict with auto generated negative scopes.
  NONE_ALL_SOME_KEYS = NONE_ALL_SOME.keys.map {|sym| sym.to_s}.freeze
  NONE_ALL_SOME_ADIF_KEYS = NONE_ALL_SOME_ADIF.keys.map {|sym| sym.to_s}.freeze

  enum land_station: NONE_ALL_SOME, _suffix: true
  validates :land_station, inclusion: {in: NONE_ALL_SOME_KEYS}

  enum one_qth: NONE_ALL_SOME, _suffix: true
  validates :one_qth, inclusion: {in: NONE_ALL_SOME_KEYS}

  enum repeater: NONE_ALL_SOME_ADIF, _suffix: true  # RPT in PROP_MODE field
  validates :repeater, inclusion: {in: NONE_ALL_SOME_ADIF_KEYS}

  enum satellite: NONE_ALL_SOME_ADIF, _suffix: true # SAT in PROP_MODE field
  validates :satellite, inclusion: {in: NONE_ALL_SOME_ADIF_KEYS}

  enum packet_radio: NONE_ALL_SOME_ADIF, _suffix: true # PKT in PROP_MODE field
  validates :packet_radio, inclusion: {in: NONE_ALL_SOME_ADIF_KEYS}

  enum internet_assisted: NONE_ALL_SOME_ADIF, _suffix: true # INTERNET in PROP_MODE field
  validates :internet_assisted, inclusion: {in: NONE_ALL_SOME_ADIF_KEYS}

  enum mobile: NONE_ALL_SOME, _suffix: true
  validates :mobile, inclusion: {in: NONE_ALL_SOME_KEYS}

  NO_PAPER = {no_info: 0, adif_qsl_sent_n: 1, remark_noqsl: 2}.freeze
  NO_PAPER_KEYS = NO_PAPER.keys.map{|sym| sym.to_s}.freeze
  enum qsl_paper: NO_PAPER
  validates :qsl_paper, inclusion: {in: NO_PAPER_KEYS}

  def may_be_changed?
    return true
  end

  private

  def set_blob_content_type
    if adif_log.attached?
      blob = adif_log.blob
      blob.content_type = "text/plain; charset=utf-8"
      blob.save!
    end
  end

  def upcase_ota
    self.gma = self.gma.upcase if self.gma.present?
    self.dlff = self.dlff.upcase if self.dlff.present?
  end

  def destroy_blob
    if adif_log.attached?
      logger.info("Deleting #{adif_log.inspect} for lfr #{id}")
      adif_log.purge
    else
      logger.warn("No adif_log, so not cleaned up for lfr #{id}.")
    end
  end

  def fiddle_reservation_on_destroy
    reservation.on_log_reset
  end

end


