class Station < ApplicationRecord
  has_many :reservations
  validates :name, :earliest_start, :latest_end, presence: true
end
