class CaptchaTemplate < ApplicationRecord

  def invalidate
    self.can_be_asked = false
    self.save
  end

  def self.give_captcha_to_ask
    # This is inefficient, CPU-wise. But it was efficient, programming time-wise.
    all_cts = CaptchaTemplate.where(can_be_asked: true).each.to_a

    # Use of the default random generator did not combine well with threads opened by Puma:
    # Early after application start, the same questions tended to be asked repeatedly.
    result = all_cts[Random.new.rand all_cts.length]

    result.asked = result.asked + 1
    begin
      result.save
    rescue ActiveRecord::StaleObjectError
      logger.warn "Captcha: Not counting one asked of #{result.id}"
    end
    result
  end

  def self.check_answer(id, answer, recursions_if_stale = 5)
    ct = CaptchaTemplate.find(id)
    ct.answered = ct.answered + 1
    regexp = Regexp.new("\\A#{ct.solution_regexp}\\z", Regexp::IGNORECASE)
    solved = regexp.match? answer
    if solved
      ct.answered_successfully = ct.answered_successfully + 1
    else
      logger.info "Captcha #{id} not solved, expected #{regexp}, saw \"#{answer}\"."
    end
    begin
      ct.save
    rescue ActiveRecord::StaleObjectError
      logger.info "Captcha: Handling a stale object error through recursion for #{ct.id}, #{solved}"
      if recursions_if_stale > 0
        self.check_answer(id, answer, recursions_if_stale - 1)
      else
        logger.warn "Captcha: Giving up on stale object error recursion for #{ct.id}, #{solved}"
      end
    end
    solved
  end
end
