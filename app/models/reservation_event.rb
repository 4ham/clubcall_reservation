class ReservationEvent < ApplicationRecord
  belongs_to :reservation
  enum event_type: [
         :requested, :canceled, :granted,
         :terminated, :no_activity,
         :log_received, :log_broken, :log_processed,
         :log_published
       ]

  enum importance: [:badly_wanted, :normal, :unimportant]

  after_create :i_am_the_last_now

  STATE2NEXT_STATES = {
    requested: [:canceled, :granted].freeze,
    canceled: [].freeze,
    granted: [:terminated, :no_activity, :log_received].freeze,
    terminated: [:log_received].freeze,
    no_activity: [].freeze,
    log_received: [:log_broken, :log_processed, :terminated].freeze,
    log_broken: [:log_received, :log_processed, :terminated].freeze,
    log_processed: [:log_published, :terminated].freeze,
    log_published: [].freeze
  }.freeze

  ADMIN_REQUIRED_TO_REACH = [
    :granted, :log_received, :log_broken, :log_processed, :log_published
  ].freeze

  STATES_REACHABLE_VIA_UI = [
    :requested, :canceled, :granted, :terminated, :no_activity
  ].freeze

  STATES_WAITING_FOR_LOGS = [
    :requested, :granted, :terminated, :log_broken
  ].freeze

  def choices_next_state(is_admin, has_log_file_records = false)
    result = if is_admin
               STATE2NEXT_STATES[event_type.to_sym]
             else
               STATE2NEXT_STATES[event_type.to_sym].find_all {|et| not ADMIN_REQUIRED_TO_REACH.include?(et)}
             end
    if has_log_file_records && STATES_WAITING_FOR_LOGS.include?(event_type.to_sym)
      result + [ :log_received ]
    else
      result
    end
  end

  def ui_choices_next_state(is_admin)
    result = choices_next_state(is_admin).
             find_all {|et| STATES_REACHABLE_VIA_UI.include? et}.
             find_all {|et| et != :terminated || reservation.start_time <= Time.now.getutc}
  end

  private

  def i_am_the_last_now
    # This is a belt-and-suspenders type code.
    # It should not help any, as the real code that does this
    # is in reservation.add_event
    reservation.reservation_events.all.each do |re|
      if re.last_for_reservation && re.id != id
        re.last_for_reservation = false
        re.save!
      end
    end
  end
end
