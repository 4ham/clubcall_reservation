class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :async, :registerable, :validatable, \
         :recoverable, :confirmable, :rememberable, :lockable
  has_one :user_preference
  has_many :reservations
  validates :callsign, \
            format: { with: /\Ad[a-r][a-z0-9]{1,20}\Z/i ,
                      message: "Kein deutsches Rufzeichen."
                    }
  before_validation :normalize_names

  def normalize_names
    self.callsign = callsign.downcase unless callsign.blank?
  end

  def has_reservations?
    not reservations.empty?
  end
end
