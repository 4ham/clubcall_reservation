# coding: utf-8
Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources :user_preferences, path: 'Fragebogen'

  root to: "static_content#show"

  devise_for :users, path: 'ops', controllers: {
               # ROOT hat einen harten Link auf /ops/registrieren/sign_up
               registrations:  'users/registrations',
               confirmations:  'users/confirmations',
             }, path_names: {
               sign_in: 'anmelden',
               sign_out: 'abmelden',
               confirmation: 'bestätigen',
               registration: 'registrieren',
               edit: 'ändern'
             }

  resources :captcha_templates

  resources :user_admin, only: [:index, :update]

  get 'emails', to: 'emails#index'
  get '4admin', to: 'status#admin'

  resources :stations, only: [:index] do
    get '4op', to: 'status#op'
    get 'upcoming', to: 'status#upcoming'
    get 'past', to: 'status#past'
    resources :reservations, only: [:index, :new, :create]
  end

  # Shallow on these:
  resources :reservations, only: [:show, :edit, :update, :destroy] do
    resources :log_file_records, only: [:index, :new, :create]
  end

  resources :log_file_records, only: [:show, :edit, :update, :destroy]

  get '(:slug)', to: "static_content#show"
end
