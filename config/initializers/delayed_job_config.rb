# Run with bundle exec rake jobs:work

Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.max_attempts = 14 # 35 hours
Delayed::Worker.default_priority = 42
Delayed::Worker.default_queue_name = "oddjob"

# Can be interrupted at any time when this is set,
# otherwise not while sleeping.
# Delayed::Worker.raise_signal_exceptions = :term
Delayed::Worker.sleep_delay = 10

inner_logger = ActiveSupport::Logger.new(STDOUT)
inner_logger.formatter = ::Logger::Formatter.new
logger = ActiveSupport::TaggedLogging.new(inner_logger)

Delayed::Worker.logger = logger
ActiveJob::Base.logger = logger

  

