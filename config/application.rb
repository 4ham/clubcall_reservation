require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Webapp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # This is for sections of the Deutscher Amateur Radio Club,
    # so if in doubt, use German locale.
    config.i18n.default_locale = :de

    config.active_job.queue_adapter = :delayed_job

  end
end

Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.default_priority = 42

module Rails
  module Rack
    class Logger
      private
      def started_request_message(request) # :doc:
        'Started %s "%s" for %s at %s' % [
          request.request_method,
          request.filtered_path,
          "DSGVO_konform",
          Time.now.to_default_s ]
      end
    end
  end
end
