# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_06_163047) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "captcha_templates", force: :cascade do |t|
    t.string "question", null: false
    t.string "solution_regexp", null: false
    t.integer "asked", default: 0, null: false
    t.integer "answered", default: 0, null: false
    t.integer "answered_successfully", default: 0, null: false
    t.boolean "can_be_asked", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["can_be_asked"], name: "index_captcha_templates_on_can_be_asked"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "log_file_records", force: :cascade do |t|
    t.bigint "reservation_id", null: false
    t.string "callsign", null: false
    t.string "locator"
    t.integer "land_station", null: false
    t.integer "one_qth", null: false
    t.integer "repeater", null: false
    t.integer "satellite", null: false
    t.string "satellite_name"
    t.integer "packet_radio", null: false
    t.integer "internet_assisted", null: false
    t.integer "mobile", null: false
    t.boolean "log_contains_irrelevant_qsos", null: false
    t.integer "qsl_paper", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "dlff"
    t.string "gma"
    t.index ["reservation_id"], name: "index_log_file_records_on_reservation_id"
  end

  create_table "reservation_events", force: :cascade do |t|
    t.bigint "station_id", null: false
    t.bigint "reservation_id"
    t.integer "event_type", null: false
    t.boolean "op_publish"
    t.integer "importance"
    t.text "comment"
    t.text "qrg_mode"
    t.datetime "decision_wanted"
    t.boolean "last_for_reservation", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reservation_id"], name: "index_reservation_events_on_reservation_id"
    t.index ["station_id", "last_for_reservation", "event_type"], name: "station_id_last_what"
    t.index ["station_id"], name: "index_reservation_events_on_station_id"
  end

  create_table "reservations", force: :cascade do |t|
    t.bigint "station_id", null: false
    t.bigint "user_id", null: false
    t.datetime "start_time", null: false
    t.datetime "end_time", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["start_time"], name: "index_reservations_on_start_time"
    t.index ["station_id", "start_time"], name: "index_reservations_on_station_id_and_start_time"
    t.index ["station_id", "user_id"], name: "index_reservations_on_station_id_and_user_id"
    t.index ["station_id"], name: "index_reservations_on_station_id"
    t.index ["user_id"], name: "index_reservations_on_user_id"
  end

  create_table "stations", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "earliest_start", null: false
    t.datetime "latest_end", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_preferences", force: :cascade do |t|
    t.bigint "user_id"
    t.boolean "anniversary_mail"
    t.boolean "general_mail"
    t.boolean "member_d"
    t.text "ov"
    t.boolean "log_provide_adif"
    t.boolean "help_qslcard"
    t.boolean "help_programming"
    t.text "question_version", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "pledge_hours_april"
    t.integer "pledge_hours_may"
    t.integer "pledge_hours_june"
    t.boolean "default_op_publish", default: false, null: false
    t.index ["user_id"], name: "index_user_preferences_on_user_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "callsign", default: "", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "verified_member", default: false, null: false
    t.boolean "is_admin", default: false, null: false
    t.index ["callsign"], name: "index_users_on_callsign", unique: true
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "log_file_records", "reservations", on_delete: :restrict
  add_foreign_key "reservation_events", "reservations"
  add_foreign_key "reservations", "stations"
  add_foreign_key "reservations", "users"
  add_foreign_key "user_preferences", "users"
end
