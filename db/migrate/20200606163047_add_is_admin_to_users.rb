class AddIsAdminToUsers < ActiveRecord::Migration[6.0]
  def up
    add_column :users, :is_admin, :boolean, default: false, null:false
    User.where(:callsign => "dj3ei").update_all(is_admin: true)
  end

  def down
    add_column :users, :is_admin, :boolean, default: false, null:false
  end
end
